#!/usr/bin/env python3
from setuptools import setup, find_packages
setup(
    name = 'pymira',
    version = '0.1.0',
    description = '',
    long_description = '',
    keywords = '',
    license = 'GPLv3',
    author = 'Gregor Möller',
    author_email = 'gregor.moeller@lmu.de',
    url = 'https://gitlab.com/MetGreg/pymira',
    classifiers = [],
    test_suite = 'tests',
    tests_require = [],
    install_requires = [],
    packages = find_packages()
)
