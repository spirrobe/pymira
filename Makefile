DOC_DIR = './docs/'
STUB_DIR = './docs/source/rst/'

.Phony: doc
doc:
			sphinx-autogen -o docs/source/rst/pymira docs/source/rst/pymira.rst
			cd $(DOC_DIR) && make html

.Phony: clean
clean:
			cd $(STUB_DIR)/pymira && rm *.rst
			cd $(DOC_DIR) && make clean


