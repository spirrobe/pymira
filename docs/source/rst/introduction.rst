Introduction
============

This is the documentation for the python package `pymira <#Documentation-of-pymira>`_. pymira is
a convenient way to steer Meteks radar mira36. The installation
of this package is described here: :doc:`installation`. The actual python
code of pymira is documented in :doc:`pymira`. In section
:doc:`examples` some example scripts are shown that demonstrate how to steer
mira36 using pymira.

.. note::
   Don't hesitate to ask if you have any questions:
   gregor.moeller@lmu.de
