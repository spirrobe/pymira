Installation
============

Pymira is available on gitlab: https://gitlab.com/MetGreg/pymira . 
It is based on python3.

Local installation
------------------

For local installation, download the package from gitlab first. 
Go to the pymira package folder and install the package with:

.. code-block:: python

   python3 setup.py install
   pip3 install -r requirements.txt

This installs the pymira package as well as the required python dependencies.

.. note::
   It is recommended to install the package in a virtual environment
   (https://virtualenv.pypa.io/en/latest/). This does not only prevent any
   dependency problems with other python package, but also works if you don't
   have sudo permissions.

For communication with mira, you need to be connected to the mira pc. If you
want to steer mira from your remote pc, you need to open a tunnel to mira36, 
e.g. by forwarding port 8000:

.. code-block:: bash

   ssh -L 8000:aerotech:8000 mira36

.. Note::

   The tunnel command above only works if you have established ProxyJumps to 
   mira36. An example of the setup that I use at LMU is below (File 'config' in
   .ssh folder). Access on mira36 via ssh keys must have been granted prior to
   this. If you have trouble setting this up, just contact me.

.. code-block:: bash

   Host radar
   Hostname radar
   User data

   Host mira36
   User data
   ProxyCommand ssh radar nc mira36 22

