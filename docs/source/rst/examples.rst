Steering scripts
================

Below you can find some example scripts that I use to steer mira. Most of them
just follow some specific scan patterns, given some command line input, such as
an azimuth angle (That was precalculated by some other script). This page is
supposed to give you an idea on the possibilities you have with pymira.


The example scripts are located within the *script*-folder within the
pymira-directory. Naturally, you can add scripts for scans that you want to
execute.

.. Note::
   You can watch the position of the antenna in real time by running the command
   *watch_pos* from the mira36 pc.


Move Home
---------

The script ``move_home.py`` simply moves the antenna into home position
(azimuth=0, elevation=90).


Home Antenna
------------

The script ``home_antenna.py`` homes the antenna and then moves the antenna back
to home position. During the home process, the antenna points towards the
horizone, so make sure the radiation is off before you execute this script.
The script asks you for confirmation, if radiation really is off.


Perform SRHI
------------

The script ``perform_srhi.py`` executes a sector RHI scan (Three RHI scans, one 
into target direction, one two degreese left, one two degrees right). The target 
azimuth angle is given as a command line argument.


PS controlling
--------------

The script ``ps_controlling.py`` reads input from a playstation controller and
translates that to aerotech steering commands. The 'pygame' library has to be
installed additionaly, for this script to work.

