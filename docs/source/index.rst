Documentation of pymira
=======================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   rst/introduction
   rst/installation
   rst/examples
   rst/pymira
   



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
