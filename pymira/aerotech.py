"""Python wrapper for communication with mira

This module allows communication with mira over simple python commands. It
is possible to connect to the aerotech controller and use some of the
aerotech commands. Almost all python commands have the same syntax as the
corresponding aerotech commands. Furthermore some basic scan strategies such as
RHI-Scans can be performed.

Note:
    The connection from a remote host is only possible when forwarding the
    localhost over a specific port (e.g. default port 8000) to mira36 and
    then starting the Aerotech connection with 'localhost' as the host.

Many thanks to Tobias Kölling for the idea of this wrapper and creating the
basic structure.

"""
import socket
import sh

ACK = '%'
NACK = '!'
FAULT = '#'
TIMEOUT = '$'


class CommandError(RuntimeError):
    """Error class for commands not understood by aerotech"""
    def __init__(self, result):
        self.result = result

    def __repr__(self):
        return "CommandError({})".format(self.result)


class ConnectedAerotech(object):
    """Implementation of the aerotech ASCII protocol

    Args:
        sock (:socket:`socket <socket>`): Socket to be used.
        axes (tuple): Tuple of (bytes, bytes) the active axes.

    Attributes:
        sock (:socket:`socket <socket>`): Socket to be used.
        axes (tuple): Tuple (bytes, bytes) of the active axes.

    """

    def __init__(self, sock, axes):
        self._sock = sock
        self.axes = axes

    def close(self):
        """Close the connection

        Must be called when controller is not used anymore!
        """
        self._sock.close()

    def exchange(self, command):
        """Send a command to the controller and return the result

        Args:
            command (str): Command to execute.

        Returns:
            (str): Response to the command.

        """
        self._sock.send((command + "\n").encode('utf-8'))
        return self._sock.recv(4096).strip().decode('utf-8')

    def do_action(self, command):
        """Execute a command without response

        Args:
             command (str): Command to execute.

        Returns:
            (ConnectedAerotech): Instance of ConnectedAerotech.

        """
        res = self.exchange(command)
        if res != ACK:
            raise CommandError(res)
        return self

    def wait_inpos(self, axis=None):
        """Wait until axis is in requested position

        Args:
            axis (str): Which axis to wait on. If no axis is given, wait for
                all axes.

        Returns:
            (ConnectedAerotech): ConnectedAerotech instance.

        """
        if axis is None:
            axis = " ".join(self.axes)
        self.do_action("wait inpos " + axis)
        return self

    def get_float(self, command):
        """Exchanges a command, expecting a float result

        Args:
            command (str): Command to execute.

        Returns:
            (float): Response as float.

        """
        res = self.exchange(command)
        if res[:1] != ACK:
            raise CommandError(res[:1].encode('utf-8'))
        return float(res[1:])

    def get_axis_float(self, command, axis=None):
        """Exchanges a command with an axis, expecting float result

        Args:
            command (str): Command to execute.
            axis (str): Active axis. If no axis is given, return a tuple
                with results for all axes.

        Returns:
            (float or tuple): Response as float or tuple of floats.

        """
        if axis is None:
            return tuple(self.get_axis_float(command, axis)
                         for axis in self.axes)
        else:
            return self.get_float(command + " " + axis)

    def get_pos(self, axis=None):
        """Get position feedback of given axis.

        Args:
            axis (str): Active axis. If no axis is given, return a tuple
                with results for all axes.

        Returns:
            (tuple or float): Position(s) of given or all axes.

        """
        return self.get_axis_float("pfbk", axis)

    def get_current(self, axis=None):
        """Gets current feedback of given axis (or all)

        Args:
            axis (str): Active axis. If no axis is given, return a tuple
                with results for all axes.

        Returns:
            (tuple or float): Returns electric current along given axis.

        """
        return self.get_axis_float("ifbk", axis)

    def get_velocity(self, axis=None):
        """Gets velocity feedback of given axis (or all)

        Args:
            axis (str): Active axis. If no axis is given, return a tuple
                with results for all axes.

        Returns:
            (float or tuple): Velocity of given axis or all axes.

        """
        return self.get_axis_float("vfbk", axis)

    def move_abs(self, **kwargs):
        """Move to absolute position

        Returns:
            (ConnectedAerotech): ConnectedAerotech instance.
        """
        command = "moveabs " + " ".join("{} {:f}".format(k, v)
                                        for k, v in kwargs.items())
        return self.do_action(command)

    def move_inc(self, **kwargs):
        """Moves incremental amount along axis

        Returns:
            (ConnectedAerotech): ConnectedAerotech instance.

        """
        command = "moveinc " + " ". join("{} {:f}".format(k, v)
                                         for k, v in kwargs.items())
        return self.do_action(command)

    def move_home(self):
        """Moves to home position

        Home position is at x=0 and y=90.

        Returns:
            (ConnectedAerotech): ConnectedAerotech instance.

        """
        command = "moveabs x 0 y 90 xf 6 yf 6"
        return self.do_action(command)

    def home(self):
        """Homes the axes

        Returns:
            (ConnectedAerotech): ConnectedAerotech instance.

        """
        command = "home x y"
        return self.do_action(command)

    def ramp_rate(self, **kwargs):
        """Defines the ramp rate

        Returns:
            (ConnectedAerotech): ConnectedAerotech instance.

        """
        command = "ramp rate " + " ". join("{} {:f}".format(k, v)
                                           for k, v in kwargs.items())
        return self.do_action(command)


class Aerotech(object):
    """Context manager for connecting with an aerotech controller

    To ensure that the connection is closed in the end,
    this class must be used in a with statement, in order to get
    access to the actual protocol interface.

    Args:
        host (str): Host name. Defaults to aerotech.
        port (int): Port used for communication with mira. Defaults to 8000.

    Attributes:
        axes (bytes): Names of axes.
        host (str): Host name.
        port (int): Port used for communication with mira.
        _conn (ConnectedAerotech): Active connection.

    """
    axes = "x", "y"

    def __init__(self, host="aerotech", port=8000):
        self.host = host
        self.port = port
        self._conn = None

    def __enter__(self):
        assert self._conn is None
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.host, self.port))
        self._conn = ConnectedAerotech(sock, self.axes)
        self._conn.ramp_rate(x=20, y=20)  # Set maximum acceleration
        return self._conn

    def __exit__(self, *args):
        self._conn.close()
        self._conn = None


class AerotechControl(object):
    """Aerotech Control Client

    Provides some methods for low level configuration, such as setting the
    number of spectra to be averaged.

    """

    def __init__(self):
        self.crctl = sh.ssh.bake('mira36', '/home/data/metek/m36s/bin/crcmd',
                                 'view_control=2')

    def set_avc_fast(self):
        """Adjust spectra averaging to fast scanning tasks

        The number of spectra averaged is put to 5.

        """
        command = '/home/data/metek/m36s/srv_scripts/set_avc_RHI_fast'
        self.crctl(command)

    def set_avc_prof(self):
        """Adjust spectra averaging to non scanning tasks

        The number of spectra averaged is put to 200.

        """
        command = '/home/data/metek/m36s/srv_scripts/set_avc_PROF'
        self.crctl(command)

    def set_pulsewidth(self, pulsewidth):
        """Sets pulsewidth and gate bin width

        Args:
            pulsewidth (int): Pulsewidth in ns.

        """
        if pulsewidth < 208 or pulsewidth > 240:
            print('Pulsewidth must be between 208 and 240 ns')
            return
        command = '/home/data/metek/m36s/srv_scripts/set_pulsewidth.cc' 
        getattr(self.crctl, command)(str(pulsewidth))

    @staticmethod
    def get_scan_processing():
        """Starts the scan processing script"""
        command = '/home/data/metek/m36s/bin/get_processing_volume_scan'
        sh.ssh.bake('mira36', command)()

    @staticmethod
    def get_default_processing():
        """Starts the default processing script"""
        command = '/home/data/metek/m36s/bin/get_processing'
        sh.ssh.bake('mira36', command)()


class AerotechScan(object):
    """Aerotech Scanning context manager

    To ensure that the average time is increased back to 10 seconds in the
    end, this class should be used in a with statement. Entering this
    class automatically decreases average spectra to 5, while exiting will
    increase them back to 200 and automatically moves the radar to the home
    position (azi=0°, ele=90°).

    Args:
        host (str): Host name. Defaults to aerotech.
        pulsewidth (int): Pulsewidth in ns.
        offset (float): Offset from radar 0 degree to north direction

    Attributes:
        crctl (:class:`AerotechControl`): Instance of AerotechControl for
            access to low level configuration commands.
        host (str): Host name.
        scan_pw (int): Scan pulsewidth in ns.
        default_pw (int): Default pulsewidth in ns.
        offset (float): Offset from radar 0 degree to north direction

    """

    def __init__(self, host='aerotech', pulsewidth=None, offset=157.3):
        self.crctl = AerotechControl()
        self.host = host
        self.scan_pw = pulsewidth
        self.default_pw = 208
        self.offset = offset

    def __enter__(self):
        self.crctl.set_avc_fast()
        if self.scan_pw:
            self.crctl.set_pulsewidth(self.scan_pw)
        self.crctl.get_scan_processing()
        return self

    def __exit__(self, *args):
        self.crctl.set_avc_prof()
        self.crctl.set_pulsewidth(self.default_pw)
        self.crctl.get_default_processing()
        with Aerotech(self.host) as aerotech:
            aerotech.move_home()

    def perform_rhi(self, azi=253.3, nr_of_scans=1, start_elv=2, stop_elv=170,
                    elvv=4):
        """Perform RHI scan

        Performs RHI scans from 2° to 170° at given azimuth angle. When no
        arguments are passed, a RHI-scan towards Poldirad is performed.

        Args:
            azi (float): Azimuth angle (north = 360°). Defaults to 253.
            nr_of_scans (int): Number of scans to be performed. Defaults to 1.
            start_elv (float): Starting elevation angle. Defaults to 2°.
            stop_elv (float): Stop elevation angle. Defaults to 170°.
            elvv (int): Elevation speed in °/s. Defaults to 4 °/s.

        Raises:
            ValueError, if start_elv or stop_elv are not between 0 and 180°.

        """
        if any((angle < 2 or angle > 178) for angle in (start_elv, stop_elv)):
            raise ValueError('Elevation angles must be between 2 and 178°')
        x = (azi + self.offset) % 360  # Radar coordinates have offset.
        with Aerotech(self.host) as aerotech:
            aerotech.move_abs(x=x, xf=6, y=start_elv, yf=6).wait_inpos()
            for i in range(nr_of_scans):
                aerotech.move_abs(y=stop_elv, yf=elvv).wait_inpos()
                aerotech.move_abs(x=x, xf=6, y=start_elv, yf=6).wait_inpos()

    def one_way_rhi(self, azi=253.3, start_elv=2, stop_elv=170, elvv=4):
        """Perform RHI scan

        Performs RHI scans from 2° to 170° at given azimuth angle. When no
        arguments are passed, a RHI-scan towards Poldirad is performed.

        Args:
            azi (float): Azimuth angle (north = 360°). Defaults to 253.
            start_elv (float): Starting elevation angle. Defaults to 2°.
            stop_elv (float): Stop elevation angle. Defaults to 170°.
            elvv (int): Elevation speed in °/s. Defaults to 4 °/s.

        Raises:
            ValueError, if start_elv or stop_elv are not between 0 and 180°.

        """
        if any((angle < 2 or angle > 178) for angle in (start_elv, stop_elv)):
            raise ValueError('Elevation angles must be between 2 and 178°')
        x = (azi + self.offset) % 360  # Radar coordinates have offset.
        with Aerotech(self.host) as aerotech:
            aerotech.move_abs(x=x, xf=6, y=start_elv, yf=6).wait_inpos()
            aerotech.move_abs(y=stop_elv, yf=elvv).wait_inpos()

    def perform_srhi(self, azis, start_elv=2, stop_elv=170, elvv=4):
        """Perform Sector RHI scans

        Performs SRHI scans from 3° to 170° at given azimuth angles. When no
        arguments are passed, a SRHI-scan towards Poldirad is performed.

        Args:
            azis (list): Azimuth angles (north = 360°).
            start_elv (float): Starting elevation angle. Defaults to 2°.
            stop_elv (float): Stop elevation angle. Defaults to 170°.
            elvv (int): Elevation speed in °/s. Defaults to 4 °/s.

        Raises:
            ValueError, if start_elv or stop_elv are not between 2 and 180°.

        """
        if any((angle < 2 or angle > 178) for angle in (start_elv, stop_elv)):
            raise ValueError('Elevation angles must be between 2 and 178°')
        xs = [(a + self.offset) % 360 for a in azis]
        with Aerotech(self.host) as aerotech:
            aerotech.move_abs(x=xs[0], xf=6, y=start_elv, yf=6).wait_inpos()
            for x in xs:
                aerotech.move_abs(y=stop_elv, yf=elvv).wait_inpos()
                aerotech.move_abs(x=x, xf=6, y=start_elv, yf=6).wait_inpos()

    def perform_ppi(self, elv=(2), start_az=0, stop_az=360, azv=4):
        """Perform PPI scan

        Performs PPI scan from start azimuth with given azimuth speed and at
        given elevation angle.

        Args:
            elv (tuple): Elevation angle of PPI. Defaults to 2°.
            start_az (float): Starting azimuth angle of PPI. Defaults to 0.
            stop_az (float): Starting azimuth angle of PPI. Defaults to 360.
            azv (float): Azimuth angle speed in °/s. Defaults to 4.

        Raises:
            ValueError, if elevation angle is not between 2 and 178°.
            ValueError, if azimuth angle is not between 0 and 360°.

        """
        inc = stop_az - start_az
        if start_az < 0 or start_az > 360:
            raise ValueError('Azimuth angles must be between 0 and 360°')
        x = (start_az + self.offset) % 360  # Correct mira offset
        with Aerotech(self.host) as aerotech:
            for i in elv:
                aerotech.move_abs(x=x, xf=6, y=i, yf=6).wait_inpos()
                aerotech.move_inc(x=inc, xf=azv).wait_inpos()
                x = x + inc
                inc = -1*inc
