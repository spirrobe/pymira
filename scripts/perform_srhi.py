import sys
from pymira.aerotech import AerotechScan


def run_rhi(azi):
    with AerotechScan('localhost') as scan:
        scan.perform_srhi(azis=[azi-2, azi, azi+2], start_elv=2, stop_elv=90)


if __name__ == '__main__':
    assert len(sys.argv) == 2, 'Number of arguments must equal 1 (' \
                               'Azimuth-angle of Poldirad)'
    azimuth = float(sys.argv[1])
    run_rhi(azimuth)
