"""Simple script to move mira to home position"""
from pymira.aerotech import Aerotech


if __name__ == '__main__':
    with Aerotech('localhost') as aero:
        aero.move_home()
