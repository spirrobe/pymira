"""Homes the antenna position and moves radar to home position afterwards"""
from pymira.aerotech import Aerotech


def check_radiation():
    return input("Radiation turned off? ['yes', 'no'] ")


if __name__ == '__main__':
    check = check_radiation()
    if check == 'yes':
        with Aerotech('localhost') as aero:
            aero.home().wait_inpos()
            aero.move_home()
