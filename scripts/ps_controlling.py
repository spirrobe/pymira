"""
Running this script allows to steer mira with a playstation controller. The
communication to the controller is based on
-- Andrew R Gross -- 2017-11-12 -- andrew@shrad.org

This program will read input from a joystick or video game controller, format
it, and then send it over a serial channel so an Arduino can receive it.
Rows that should be customized are commented with '#-# '.

Meanwhile, a connection to mira can be started and the controller buttons
are translated to mira steering commands.

"""
import pygame
import time
import numpy as np
from pymira.aerotech import Aerotech


STEP = 1  # Angle step, with which the radar will be moved


# =============================================================================
# 1: Identify the controller
# =============================================================================
pygame.init()  # Initiate the pygame functions
j = pygame.joystick.Joystick(0)  # Define a joystick object to read from
j.init()  # Initiate the joystick or controller
print('Detected controller : %s' % j.get_name())  # Print name of controller


# =============================================================================
# 3: Select which axes to check
# =============================================================================
axes_to_check = [0, 1]  # -# List the axes # to
check_frequency = 20  # ~# Specify how many times a second to check
breakout_button = 9  # -# Specify which button stops the program


# =============================================================================
# 4: Begin reading the controller
# =============================================================================
with Aerotech("localhost") as aerotech:

    # The program will check the button and joystick states until interrupted
    while True:

        # The event.pump function refreshes knowledge of what events changed
        pygame.event.pump()
        recent_values = {}

        for current_axis in axes_to_check:  # Loop through the axes to check
            latest_value = j.get_axis(current_axis)  # Store the current value
            recent_values[current_axis] = latest_value

            # If either axis isn't centered, the value will be reported
            if latest_value != 0.00:
                pass

        # Put maximum speed to 6°, get elev-pos and sign of movement
        xf = recent_values[0]*4
        yf = recent_values[1]*(-4)
        x = np.sign(xf) * STEP
        y = np.sign(yf) * STEP
        pos_x, pos_y = aerotech.get_pos()
        pos_y_new = pos_y + y

        # Relative direction changes when over 90° Elevation
        if pos_y > 90:
            pos_x_new = pos_x - x
        else:
            pos_x_new = pos_x + x
        print(pos_y_new)
        print(pos_x_new)

        # Make actual movement (only when movement not 0)
        if xf and yf:
            if (pos_y_new > 3 or y > 0) and (pos_y_new < 177 or y < 0):
                aerotech.move_abs(
                    x=pos_x_new, y=pos_y_new, xf=abs(xf), yf=abs(yf))
        elif xf:
            aerotech.move_abs(x=pos_x_new, xf=abs(xf))
        elif yf:
            if (pos_y_new > 3 or y > 0) and (pos_y_new < 177 or y < 0):
                aerotech.move_abs(y=pos_y_new, yf=abs(yf))

        # The program waits the specified length before checking new values
        time.sleep(1/check_frequency)

        # If the breakout button is being held, the program ends
        if j.get_button(breakout_button) == 1:
            aerotech.move_home()
            break
